# PDF generation with Gottenberg

URL: https://thecodingmachine.github.io/gotenberg/

Docker image: `thecodingmachine/gotenberg:6`

Prerequisites: 
- Run docker container with port 3000 exposed: `docker run --rm -p 3000:3000 thecodingmachine/gotenberg:6`

Then run the main.go file - see the result: generated.pdf

Happy hcking!