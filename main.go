package main

import (
    "github.com/thecodingmachine/gotenberg-go-client/v7"
    "github.com/boombuler/barcode/ean"
    "bytes"
    "image/png"
    "github.com/boombuler/barcode"
    "html/template"
    "path/filepath"
    "io/fs"
    "path"
)

type TemplateVars struct {
    CardName        string
    Barcode         string
    FormattedAmount string
    Validity        string
    PartnerName     string
    PartnerWebsite  string
    PaymentDate     string
}

func createAssets(dir string) ([]gotenberg.Document, error) {
    var assets []gotenberg.Document

    err := filepath.Walk(dir, func(p string, info fs.FileInfo, err error) error {
        if !info.IsDir() {
            if path.Ext(p) != ".html" {
                a, _ := gotenberg.NewDocumentFromPath(path.Base(p), p)
                assets = append(assets, a)
            }
        }

        return nil
    })

    if err != nil {
        return nil, err
    }

    return assets, nil
}

func main() {
    vars := TemplateVars{
        CardName:        "Teszt Ajándék",
        Barcode:         "1845678901001",
        FormattedAmount: "18.000 HUF",
        Validity:        "2021.12.31",
        PartnerName:     "Guana Kimboko",
        PartnerWebsite:  "guanakimboko.com",
        PaymentDate:     "2021.06.12",
    }

    // create the client.
    client := &gotenberg.Client{Hostname: "http://localhost:3000"}

    tpl := template.Must(template.ParseFiles("./templates/index.html"))

    var bb bytes.Buffer
    err := tpl.Execute(&bb, vars)
    if err != nil {
        panic(err)
    }

    index, _ := gotenberg.NewDocumentFromBytes("index.html", bb.Bytes())
    assets, err := createAssets("./templates")
    if err != nil {
        panic(err)
    }

    eanCode, _ := gotenberg.NewDocumentFromBytes("barcode.png", createBarcode(vars.Barcode))
    assets = append(assets, eanCode)

    req := gotenberg.NewHTMLRequest(index)

    req.Assets(assets...)
    req.PaperSize(gotenberg.A4)
    req.Margins(gotenberg.NoMargins)

    // store method allows you to... store the resulting PDF in a particular destination.
    err = client.Store(req, "generated.pdf")
    if err != nil {
        panic(err)
    }

    // if you wish to redirect the response directly to the browser, you may also use:
    //resp, _ := client.Post(req)
}

func createBarcode(code string) []byte {
    // Create the barcode
    eanCode, _ := ean.Encode(code)

    // Scale the barcode to 200x200 pixels
    dst, err := barcode.Scale(eanCode, 300, 100)
    if err != nil {
        panic(err)
    }

    var bb bytes.Buffer

    // encode the barcode as png
    err = png.Encode(&bb, dst)
    if err != nil {
        panic(err)
    }

    return bb.Bytes()
}
