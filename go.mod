module go.pirat.app/pi/reporter

go 1.16

require (
	github.com/boombuler/barcode v1.0.1
	github.com/thecodingmachine/gotenberg-go-client/v7 v7.2.0
)
